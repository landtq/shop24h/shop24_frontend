import { GOOGLE_LOGIN, INPUT_PASSWORD, INPUT_USER_NAME } from "../constants/Input";

const iniData = {
    userName: "",
    password: "",
    googleUser: null
}


const LogInReducer = (state = iniData, action) => {
    switch (action.type) {
        case INPUT_USER_NAME:
            return {
                userName: action.payload
            }
        case INPUT_PASSWORD:
            return {
                password: action.payload
            }
        case GOOGLE_LOGIN:
            return {
                ...state,
                googleUser: action.payload
            }

        default:
            return state;
    }
}

export default LogInReducer