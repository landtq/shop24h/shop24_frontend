import { combineReducers } from "redux";
import ListProductReducer from "./ListProductReducer";
import LogInReducer from "./LogInReducer";
import ProductDetail from './ProductDetail'
import FilterReducer from './FilterReducer'
import CheckOutReducer from "./CheckOutReducer";
import MyOrderReducer from "./MyOrdeReducer";

const rootReducer = combineReducers({
    LogInReducer,
    ListProductReducer,
    ProductDetail,
    FilterReducer,
    CheckOutReducer,
     MyOrderReducer
})

export default rootReducer;