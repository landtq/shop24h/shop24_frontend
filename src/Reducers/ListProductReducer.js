import { FILTER_PRODUCT, GET_DATA_PRODUCT, GET_DATA_PRODUCT_HOME, PAGINATION } from "../constants/Product";

const iniData = {
    products: [],
    currentPage: 1,
    noPage: 0,
}

const limit = 6;

const ListProductReducer = (state = iniData, action) => {
    switch (action.type) {
        case GET_DATA_PRODUCT_HOME:
            return {
                ...state,
                products: action.payload
            }
        case GET_DATA_PRODUCT:
            return {
                ...state,
                products: action.payload.slice((state.currentPage - 1) * limit, state.currentPage * limit),
                noPage: Math.ceil(action.payload.length / limit)
            }
        case PAGINATION:
            return {
                ...state,
                currentPage: action.payload
            }
        case FILTER_PRODUCT:
            return {
                ...state,
                products: action.payload.product,
                noPage: Math.ceil(action.payload.total / action.payload.limit),
            }
        default:
            return state;
    }
}

export default ListProductReducer;
