import {  CREATE_NEW_ORDER } from "../constants/CheckOutConst";

const initialData = {
    ordered: [],
}
const CheckOutReducer = (state = initialData, action) => {
    switch (action.type) {
        case CREATE_NEW_ORDER:
            return {
                ...state,
                ordered: action.payload
            }
        default:
            return state;
    }
}

export default CheckOutReducer;