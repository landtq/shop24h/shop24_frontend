import { FILTER_NAME, FILTER_MAX_PRICE, FILTER_MIN_PRICE, FILTER_CATEGORIES, FILTER_COLOR } from '../constants/Filter'


const iniData = {
    filterMaxPrice: 350,
    filterMinPrice: 0,
    filterName: "",
    filterCategories: "",
    filterColor: ""
}

const FilterReducer = (state = iniData, action) => {
    switch (action.type) {
        case FILTER_NAME:
            return {
                ...state,
                filterName: action.payload
            }
        case FILTER_MAX_PRICE:
            return {
                ...state,
                filterMaxPrice: action.payload
            }
        case FILTER_MIN_PRICE:
            return {
                ...state,
                filterMinPrice: action.payload
            }
        case FILTER_CATEGORIES:
            return {
                ...state,
                filterCategories: action.payload
            }
        case FILTER_COLOR:
            return {
                ...state,
                filterColor: action.payload
            }
        default:
            return state;
    }
}

export default FilterReducer;