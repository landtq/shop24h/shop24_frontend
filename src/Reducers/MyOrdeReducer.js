import { GET_LIST_ORDER } from "../constants/MyOrder";

const initialData = {
    orderList: "",
}
const MyOrderReducer = (state = initialData, action) => {
    switch (action.type) {
        case GET_LIST_ORDER:
            return {
                ...state,
                orderList: action.payload
            }
        default:
            return state;
    }
}

export default MyOrderReducer;