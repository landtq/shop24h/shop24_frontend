import { GET_DATA_PRODUCT_BY_ID } from "../constants/Product";

const iniData = {
    product: "",
}


const ProductDetail = (state = iniData, action) => {
    switch (action.type) {
        case GET_DATA_PRODUCT_BY_ID:
            return {
                ...state,
                product: action.payload
            }

        default:
            return state;
    }
}

export default ProductDetail;
