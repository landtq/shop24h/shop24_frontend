import { Alert, Snackbar, Stack } from "@mui/material";
import { useState } from "react";
import { Form, InputGroup } from "react-bootstrap";

const Subcrible = () => {
    const [displayAlert, setDisplayAlert] = useState(false);
    const [typeAlert, settypeAlert] = useState("error");
    const [textAlert, settextAlert] = useState("text");

    const closeAlert = () => {
        setDisplayAlert(false);
    }

    const [email, setEmail] = useState('')
    const changeEmail = (value) => {
        setEmail(value.target.value)
    }
    const onSubcribleBtn = () => {
        const reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (email.match(reg)) {
            setDisplayAlert(true);
            settypeAlert("success");
            settextAlert("Thanks for your subcribe");
            setEmail("")
        } else {
            setDisplayAlert(true);
            settypeAlert("error");
            settextAlert("Your email invalid");
            setEmail("")
        }
    }
    return (
        <div className="subcribe">
            <div className="row ">
                <div className="col-sm-6">
                    <h1 style={{ fontWeight: 'bold', color: 'white' }}>Subscribe for a <span style={{ color: "orange" }}> 25% Discount</span></h1>
                    <h5 className="mt-3" style={{ color: "white", opacity: '0.5' }}>Nulla ac convallis lorem, eget . Donec in libero sit amet mi vulputate conur. Donec auctor interdum purus, ac finibus massa bibendum nec.</h5>
                </div>
                <div className="col-sm-1"></div>
                <div className="col-sm-4">
                    <InputGroup className="mt-5" >
                        <Form.Control
                            placeholder="Your E-mail"
                            onChange={changeEmail}
                            value={email}
                        />
                        <InputGroup.Text style={{ backgroundColor: "orange", cursor: "pointer" }} onClick={onSubcribleBtn}>Subcrible</InputGroup.Text>
                    </InputGroup>
                </div>
            </div>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>
        </div>
    )
}
export default Subcrible;