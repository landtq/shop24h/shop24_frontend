import { Button, Form, InputGroup } from "react-bootstrap";
import { Alert, Snackbar, Stack } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import LogInForm from "./LogInForm";
import Header from "./Header";


import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation, Thumbs } from 'swiper'
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/thumbs'
import './slide.scss'
import { useDispatch, useSelector } from "react-redux";
import { getListData } from "../actions/getListDataAction";

const Cart = () => {
    const navigate = useNavigate();
    const [totalItem, setTotalItems] = useState(0);
    const [totalPrice, setTotalPrice] = useState(0);
    const [delivery, setDelivery] = useState(0);
    const [tax, setTax] = useState(0);
    const { products } = useSelector((data) => data.ListProductReducer)
    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [typeAlert, settypeAlert] = useState("error");
    const [textAlert, settextAlert] = useState("text");

    const [modalSign, setModal] = useState(false);

    const closeAlert = () => {
        setDisplayAlert(false);
    }
    const closeModal = () => {
        setModal(false)
    }

    var user = JSON.parse(localStorage.getItem('user'))
    //cart
    var cartParse = localStorage.getItem('cart');
    var carts = []
    if (cartParse !== null && cartParse !== [] && cartParse !== "" && cartParse.length !== 0) {
        carts = JSON.parse(cartParse)
    } else { }

    const [cartItem, setCartItem] = useState([])

    const [email, setEmail] = useState('')
    const changeEmail = (value) => {
        setEmail(value.target.value)
    }
    const onSubcribleBtn = () => {
        const reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (email.match(reg)) {
            setDisplayAlert(true);
            settypeAlert("success");
            settextAlert("Thanks for your subcribe");
            setEmail("")
        } else {
            setDisplayAlert(true);
            settypeAlert("error");
            settextAlert("Your email invalid");
            setEmail("")
        }
    }

    const tangSoLuong = (value) => {
        const exist = carts.find((itemmm) => itemmm._id === value._id)
        if (exist) {
            const newCart = carts.map((item) => item._id === value._id ? { ...exist, qty: exist.qty + 1 } : item);
            setCartItem(newCart)
            localStorage.setItem('cart', JSON.stringify(newCart))
        } else {
            const newCart = [...carts, { ...value, qty: 1 }]
            setCartItem(newCart)
            localStorage.setItem('cart', JSON.stringify(newCart))
        }
    }
    //minus + delete 
    const giamSoluong = (value) => {
        const exist = carts.find((itemmm) => itemmm._id === value._id)
        if (exist.qty === 1) {
            const newCart = carts.filter((item) => item._id !== value._id);
            setCartItem(newCart)
            localStorage.setItem('cart', JSON.stringify(newCart))
        } else {
            const newCart = carts.map((item) => item._id === value._id ? { ...exist, qty: exist.qty - 1 } : item);
            setCartItem(newCart)
            localStorage.setItem('cart', JSON.stringify(newCart))
        }
    }

    //check out
    const handleCheckoutBtn = () => {
        if (totalPrice > 0 && user !== null) {
            navigate(`/checkout`)
        } else if (totalPrice <= 0) {
            setDisplayAlert(true);
            settypeAlert("error")
            settextAlert("Please select some product options add to your cart before check out")
        } else if (user === null || user === [] || !user) {
            setDisplayAlert(true);
            settypeAlert("error")
            settextAlert("You must be logged in to checkout")
            setModal(true)
        }
    }

    useEffect(() => {
        let items = 0;
        let price = 0;
        let deliverys = 0;
        let taxs = 0
        carts.forEach(
            item => {
                items += item.qty;
                price += item.qty * item.buyPrice
            })
        if (price > 1000) { deliverys = 0 } else {
            deliverys = 2 * items;
            setDelivery(deliverys)
        }
        taxs = price * 0.1
        setTax(taxs)
        setTotalItems(items);
        setTotalPrice(price + deliverys + taxs)
        var cartTotal = {
            "taxxx": taxs,
            "itemsss": items,
            "deliveryyy": deliverys,
            "priceee": price
        }
        localStorage.setItem('cartTotal', JSON.stringify(cartTotal))
    }, [totalItem, setTotalItems, totalPrice, setTotalPrice, carts, cartItem, delivery, setDelivery, tax, setTax, user])
    //

    const dispatch = useDispatch()
    const randomIndex = () => {
        for (let i = 0; i < 5; i++) {
            let num = Math.floor(Math.random() * 10) + 1;
            return num
        }

    }
    useEffect(() => {
        dispatch(getListData())
    }, [])

    return (
        <>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>
            <div className="row" >
                <Header />
                <div className="col-sm-5 largeCart" style={{ margin: "100px 20px 0 0" }}>
                    <h1 className="shopcart">Shopping Cart </h1>
                    <div className="row bg-light mt-5 nameCol">
                        <div className="col-sm-3"></div>
                        <div className="col-sm-4">
                            <p>Name</p>
                        </div>
                        <div className="col-sm-2">
                            <p>Price</p>
                        </div>
                        <div className="col-sm-3">
                            <p>Quantity</p>
                        </div>
                    </div>

                    <div className="row mt-5">
                        {carts.length > 0 ? carts.map((item, index) => {
                            return (
                                <div key={index} className="row p-4">
                                    <div className="col-3 cartImg">
                                        <img src={item.imageUrl} style={{ width: "100%", border: "1px solid white", height: "150px" }} alt="img" />
                                    </div>
                                    <div className="col-4 mt-5">
                                        <h5>{item.name}</h5>
                                    </div>
                                    <div className="col-2 mt-5 priceCol">
                                        <h5>${item.buyPrice}</h5>
                                    </div>
                                    <div className="col-3 mt-5 ">
                                        <div className="row qtyColum">
                                            <div className="col-4 bg-light qtyCol">
                                                <h5 >Qty</h5>
                                            </div>
                                            <div className="col-2 bg-light">
                                                <i className="fa-solid fa-plus" onClick={() => tangSoLuong(item)}></i>
                                            </div>
                                            <div className="col-3 bg-light">
                                                <h4 >{item.qty}</h4>
                                            </div>
                                            <div className="col-2 bg-light">
                                                <i className="fa-solid fa-minus" onClick={() => giamSoluong(item)}></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                            : <h5 className="text-center mt-5"> Cart is Empty {`<`} <a href="/products" style={{ color: "orange" }}> buy now</a></h5>
                        }
                        {/* <div className="mt-5">
                            <div style={{ display: "flex", justifyContent: "space-between" }} >
                                <div className="headerTitle">
                                    Super discount
                                </div>
                                <div >
                                    <a href="/" style={{ color: "orange" }}>View all</a>
                                </div>
                            </div>
                            <div className="mt-5">
                                <Swiper
                                    spaceBetween={5}
                                    slidesPerView={4}
                                    navigation
                                    onSlideChange={() => console.log('slide change')}
                                    onSwiper={(swiper) => console.log(swiper)}
                                    pagination={{ clickable: true }}
                                    scrollbar={{ draggable: true }}

                                // loop={true}
                                >
                                    <SwiperSlide>
                                        <div class="card" style={{ width: "100%" }}>
                                            <img className="card-img-top product-images-slider-thumbs-New-wrapper" src={products.length > 10 ? products[randomIndex()].imageUrl : "err"} alt="Card image cap" />
                                            <div className="card-body">
                                                <p style={{ color: "white", backgroundColor: {} , marginTop: "-10px"}}>  <i className="fa-solid fa-tag" ></i>{ } 8 % discount</p>
                                                <p className="card-text">Some quick example text to build.</p>
                                                <h5>$3762</h5>
                                            </div>
                                        </div>
                                    </SwiperSlide>
                                    <SwiperSlide>
                                        <div className="product-images-slider-thumbs-New">
                                            <img className="product-images-slider-thumbs-New-wrapper" src={products.length > 10 ? products[randomIndex()].imageUrl : "err"} />
                                            <p>adfsfds</p>
                                        </div>
                                    </SwiperSlide>
                                    <SwiperSlide>Slide 2</SwiperSlide>
                                    <SwiperSlide>Slide 3</SwiperSlide>
                                    <SwiperSlide>Slide 4</SwiperSlide>
                                    <SwiperSlide>Slide 4</SwiperSlide>
                                </Swiper>
                            </div>
                        </div> */}
                    </div>
                </div>

                <div className="col-3 totalInfo" >
                    <div className="container bg-light float-left" style={{ padding: "30px 50px 20px 20px", margin: "200px 0 0 30px" }}>
                        <h3> Cart Total</h3>
                        <div className="row mt-5">
                            <div className="col-sm-9">
                                <h6> Items: </h6>
                            </div>
                            <div className="col-sm-3">
                                <h6>  {totalItem}</h6>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-sm-9">
                                <h6> Tax Price </h6>
                            </div>
                            <div className="col-sm-3">
                                <h6> ${tax}.0</h6>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-sm-9">
                                <h6> Delivery: </h6>
                            </div>
                            <div className="col-sm-3">
                                <h6> ${delivery}.0</h6>
                            </div>
                            {/* <p style={{opacity: "0.5", fontSize: "14px"}}> *Add at least $1000 for free shipping</p> */}
                        </div>
                        <div className="row mt-5">
                            <div className="col-sm-9">
                                <h6> TOTAL:  </h6>
                            </div>
                            <div className="col-sm-3">
                                <h6>
                                    $ {totalPrice}.0
                                </h6>
                            </div>
                        </div>
                        <div className="row">
                            <Button onClick={handleCheckoutBtn} style={{ width: "90%", height: '60px', color: "white", fontSize: "25px", margin: "80px 0 0 40px" }} variant="warning">Checkout</Button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="subcribe">
                <div className="row ">
                    <div className="col-sm-6">
                        <h1 style={{ fontWeight: 'bold', color: 'white' }}>Subscribe for a <span style={{ color: "orange" }}> 25% Discount</span></h1>
                        <h5 className="mt-3" style={{ color: "white", opacity: '0.5' }}>Nulla ac convallis lorem, eget . Donec in libero sit amet mi vulputate conur. Donec auctor interdum purus, ac finibus massa bibendum nec.</h5>
                    </div>
                    <div className="col-sm-1"></div>
                    <div className="col-sm-4">
                        <InputGroup className="mt-5" size="lg">
                            <Form.Control
                                placeholder="Your E-mail"
                                onChange={changeEmail}
                                value={email}
                            />
                            <InputGroup.Text style={{ backgroundColor: "orange", cursor: "pointer" }} onClick={onSubcribleBtn}>Subcrible</InputGroup.Text>
                        </InputGroup>
                    </div>
                </div>
            </div>
            <LogInForm modalSign={modalSign}
                setModal={setModal}
                closeModal={closeModal} />
        </>
    )
}
export default Cart;