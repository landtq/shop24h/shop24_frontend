import { ArrowForwardIos } from "@mui/icons-material";
import { Breadcrumbs, Link, Typography } from "@mui/material";

const BreakCrumbAllPages = ({ productList, nameDeatil, type }) => {
    const handleClick = (event) => {
        console.info('breadcrumbs is clicked');
    }
    const breadcrumbs = [
        <Link underline="hover" key="1" color="inherit" href="/" onClick={handleClick} style={{ fontSize: "20px", color: "black", opacity: '0.9' }}>
            HOME
        </Link>,
        <Link
            underline="hover"
            key="2"
            // color="inherit"
            href="/products"
            onClick={handleClick}
            style={{ fontSize: "20px", color: "black", opacity: '0.9' }}
        >
            {productList}
        </Link>,
        <Link
            underline="hover"
            key="2"
            href="/products"
            onClick={handleClick}
            style={{ fontSize: "20px", color: "black", opacity: '0.9', textTransform: "uppercase" }}
        >
            {type}
        </Link>,
        <Typography key="3" color="text.primary" style={{ fontSize: "20px", opacity: '0.5', textTransform: "uppercase" }}>
            {nameDeatil}
        </Typography>,
    ];
    return (
        <>
            <Breadcrumbs
                separator={<ArrowForwardIos fontSize="small" />}
                aria-label="breadcrumb"
                style={{ margin: "120px 0 30px 0" }}
            >
                {breadcrumbs}
            </Breadcrumbs>
        </>
    )
}
export default BreakCrumbAllPages;