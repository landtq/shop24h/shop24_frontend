
const Footer = () => {
    return (
        <>
            <footer style={{ backgroundColor: "#212529", color: "white", padding: '5% 10% 5% 10%' }}>
                <div style={{ display: "flex", justifyContent: "space-between" }}>
                    <div className="col-sm-4 contentFooter">
                        <img src="https://firebasestorage.googleapis.com/v0/b/dev-test-7b564.appspot.com/o/logo2.png?alt=media&token=012e433a-bb48-412e-93af-f580c7af4450"></img>
                        <p className="mt-3" style={{ opacity: '0.5' }}>
                            Copyright &copy;
                            <script>document.write(new Date().getFullYear());</script> All rights reserved |dtql| This
                            template is made with <i className="fa-regular fa-heart" aria-hidden="true"></i> by ndt <a
                                href="https://colorlib.com" target="_blank">Colorlib</a> & Re-distributed by <a
                                    href="https://themewagon.com/" target="_blank">Themewagon</a>
                        </p>
                    </div>
                    <div className="referentInfo  row col-sm-7" >
                        <div className="col-4">
                            <h5><a>PRODUCT</a></h5>
                            <h6 className="mt-3"><a>Help</a></h6>
                            <h6><a>Contact</a></h6>
                            <h6><a>Terms</a></h6>
                            <h6><a>Meetups</a></h6>
                        </div>
                        <div className="col-4">
                            <h5><a>SHOP</a></h5>
                            <h6 className="mt-3"><a>Privacy</a></h6>
                            <h6><a>Testimonials</a></h6>
                            <h6><a>Handbook</a></h6>
                            <h6><a>Help Desk</a></h6>
                        </div>
                        <div className="col-4">
                            <h5><a>QUALITY</a></h5>
                            <h6 className="mt-3"><a>Designers</a></h6>
                            <h6><a>Teams</a></h6>
                            <h6><a>Advertise</a></h6>
                            <h6><a>Warranty</a></h6>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}
export default Footer;