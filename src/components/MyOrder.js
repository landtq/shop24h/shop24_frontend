import { Paper, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListOrder } from "../actions/MyOrderAction";
import moment from 'moment'
import Header from "./Header";
import './style.css'

const MyOrder = () => {
    var dispatch = useDispatch()
    const { orderList } = useSelector((reduxData) => reduxData.MyOrderReducer);
    var stt = 1
    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    useEffect(() => {
        dispatch(getListOrder())
    }, [])
    return (
        <>
            <div className="row" >
                <Header />
                <div className="col-sm-9 bg-light p-5 myRecentOr">
                    <h1 className="myorder">Recent Orders</h1>
                    <TableContainer component={Paper} className="mt-5">
                        <Table aria-label="customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell align="right">No.</StyledTableCell>
                                    <StyledTableCell align="center">Order Date</StyledTableCell>
                                    <StyledTableCell align="center">ID</StyledTableCell>
                                    <StyledTableCell align="center">Total Price</StyledTableCell>
                                    <StyledTableCell align="center">Status</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {orderList ? orderList.map((row) => (
                                    <StyledTableRow key={row._id}>
                                        <StyledTableCell align="right">{stt++}</StyledTableCell>
                                        <StyledTableCell align="center">{moment(row.orderDate).format('MMMM Do YYYY, h:mm:ss a')}</StyledTableCell>
                                        <StyledTableCell align="center">{row._id}</StyledTableCell>
                                        <StyledTableCell align="center">${row.cost}.0</StyledTableCell>
                                        <StyledTableCell align="center" sx={{ color: "orange" }}>{(moment(row.orderDate).format('YYYY/MM/DD')).valueOf() >= (moment(new Date).format('YYYY/MM/DD')).valueOf() ? "deliveried" : "processing"}</StyledTableCell>
                                    </StyledTableRow>
                                )) : null}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            </div>
        </>
    )
}
export default MyOrder;