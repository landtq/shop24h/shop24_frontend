import BreakCrumbAllPages from "./BreakCrumbAllPages";
import { Button } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { ProductDetailAction } from "../actions/ProductDetailAction";
import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation, Thumbs } from 'swiper'
import './slide.scss'
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/thumbs'
import Header from "./Header";
import Subcrible from "./Subcribe";

const ProductDetail = () => {
  const dispatch = useDispatch()
  const [soLuong, setSoLuong] = useState(1);

  const [cartItem, setCartItem] = useState([])
  const { product } = useSelector((reduxData) => reduxData.ProductDetail);
  const { paramId } = useParams()
  const [activeThumb, setActiveThumb] = useState()

  //cart
  var cartParse = localStorage.getItem('cart');
  var carts = []
  if (cartParse !== null && cartParse !== [] && cartParse !== "" && cartParse.length !== 0) {
    carts = JSON.parse(cartParse)
  } else { console.log(cartParse) }

  //tang giam soluong
  const tangSoLuong = () => {
    setSoLuong(soLuong + 1)
  }
  const giamSoluong = () => {
    setSoLuong(soLuong - 1)
  }

  // nhan Add to cart
  const onAddToCartBtn = () => {
    const exist = carts.find((itemmm) => itemmm._id === product._id)
    if (exist) {
      const newCart = carts.map((item) => item._id === product._id ? { ...exist, qty: exist.qty + soLuong } : item);
      setCartItem(newCart)
      localStorage.setItem('cart', JSON.stringify(newCart))
    } else {
      const newCart = [...carts, { ...product, qty: soLuong }]
      setCartItem(newCart)
      localStorage.setItem('cart', JSON.stringify(newCart))
    }
  }

  useEffect(() => {
    dispatch(ProductDetailAction(paramId))
  }, [paramId])

  return (
    <>
      <div className="row" >
        <Header />
        <div className="col-sm-5 imgDetailStyle" >
          <BreakCrumbAllPages nameDeatil={product.name} type={product ? product.type.name : null} />
          <Swiper
            loop={true}
            spaceBetween={10}
            navigation={true}
            modules={[Navigation, Thumbs]}
            grabCursor={true}
            className='product-images-slider'
            thumbs={{ swiper: activeThumb && !activeThumb.destroyed ? activeThumb : null }}
          >
            {
              product !== "" ? product.imageChild.map((item, index) => (
                <SwiperSlide key={index}>
                  <img src={item} alt="product images" />
                </SwiperSlide>
              ))
                : null
            }
          </Swiper>
          <Swiper
            onSwiper={setActiveThumb} //
            loop={true}
            spaceBetween={10}
            slidesPerView={4}
            modules={[Navigation, Thumbs]}
            className='product-images-slider-thumbs'
          >
            {
              product !== "" ? product.imageChild.map((item, index) => (
                <SwiperSlide key={index}>
                  <div className="product-images-slider-thumbs-wrapper">
                    <img src={item} alt="product images" />
                  </div>
                </SwiperSlide>
              ))
                : null}
          </Swiper>
        </div>

        <div className="col-sm-4 detail" >
          <div className="lineDetail" ></div>
          <h2 style={{ color: "orange" }}><b>${product.buyPrice}</b> </h2>
          <h1><b>{product.name}</b></h1>
          <div >
            <img src="https://img.icons8.com/emoji/20/000000/star-emoji.png" alt="1." />
            <img src="https://img.icons8.com/emoji/20/000000/star-emoji.png" alt="2." />
            <img src="https://img.icons8.com/emoji/20/000000/star-emoji.png" alt="3." />
            <img src="https://img.icons8.com/emoji/20/000000/star-emoji.png" alt="4." />
            <img src="https://img.icons8.com/emoji/20/000000/star-emoji.png" alt="5." />
            {/* <span style={{ marginLeft: "260px", fontSize: "20px" }}>  write a comment</span> */}
          </div>
          <h5 className="mt-4"><img src="https://img.icons8.com/external-sbts2018-flat-sbts2018/20/000000/external-active-basic-ui-elements-2.3-sbts2018-flat-sbts2018.png" alt="" /> in stock</h5>
          <h4 className="description" ><>{product.description}</></h4>

          <div className="row mt-5 countDetail">
            <div className="col-2 bg-light">
              <h4 className="qty">Qty</h4>
            </div>
            <div className="col-1 bg-light ">
              <h4 className="qty">{soLuong}</h4>
            </div>
            <div className="col-1 bg-light">
              <i className="fa-solid fa-plus" onClick={tangSoLuong}></i><i className="fa-solid fa-minus" onClick={giamSoluong}></i>
            </div>
          </div>
          <div className="row mt-5">
            <Button onClick={onAddToCartBtn} className="addBtn" variant="warning"> Add to cart</Button>
          </div>
        </div>

      </div>
      <Subcrible />
    </>
  )
}
export default ProductDetail;