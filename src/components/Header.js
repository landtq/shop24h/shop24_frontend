import { GoogleLogin } from "../actions/InputAction"
import { signOut, onAuthStateChanged } from "firebase/auth";
import auth from "../firebase";
import LogInForm from "./LogInForm";
import { Button } from "react-bootstrap";
import { Menu, MenuItem } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import './style.css'
import { useNavigate } from "react-router-dom";

const Header = () => {
    const dispatch = useDispatch()

    var user = JSON.parse(localStorage.getItem('user'))
    //cart
    var cartParse = localStorage.getItem('cart');
    var carts = []
    if (cartParse !== null && cartParse !== [] && cartParse !== "" && cartParse.length !== 0) {
        carts = JSON.parse(cartParse)
    } else { }

    const [cartCount, setCartCount] = useState(0);
    const [modalSign, setModal] = useState(false);
    const listMenu = [
        { name: "HOME", url: "/" },
        { name: "SHOP", url: "/products" },
        { name: "CART", url: "/cart" },
    ];
    const closeModal = () => {
        setModal(false)
    }
    const iconContact = [
        { icon: "fa-brands fa-facebook" },
        { icon: "fa-brands fa-instagram" },
        { icon: "fa-brands fa-twitter" },
        { icon: "fa-brands fa-pinterest-p" }
    ]

    var path = window.location.pathname
    if (path === '/') {
    }

    const loginClick = () => {
        setModal(true)
    }
    const logoutGoogle = () => {
        signOut(auth)
            .then(() => {
                dispatch(GoogleLogin(null))
                localStorage.setItem('user', null)
            })
            .catch((error) => {
                console.error(error);
            })
    }

    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    var navigate = useNavigate()
    const handleSearch = () => {
        navigate("/products/#searchName")
    }

    useEffect(() => {
        onAuthStateChanged(auth, (result) => {
            if (result) {
                dispatch(GoogleLogin(result));
                localStorage.setItem('user', JSON.stringify(result))
            } else {
                dispatch(GoogleLogin(null))
            }
        })
    }, [user]);

    useEffect(() => {
        let count = 0;
        carts.forEach(item => {
            return count += item.qty
        });
        setCartCount(count)
    }, [carts])

    return (
        <div className="col-sm-3 homeCover" >
            <div className="row ">
                <a href="/">  <img className="mt-5 w-50" alt="" src="https://firebasestorage.googleapis.com/v0/b/dev-test-7b564.appspot.com/o/logo.png?alt=media&token=01487e41-59f1-49d0-bca9-ae1e09ab1d3b" /></a>
            </div>
            <div className="row navStyle" >
                <h5 className="linkStyle" style={{ display: "flex", alignItems: "center" }}>
                    {user !== null ? <img src={user.photoURL} className="userImg" alt="" /> : <i className="fa-solid fa-user"></i>}
                    <Button
                        variant="text"
                        aria-controls={open ? 'basic-menu' : undefined}
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        onClick={handleClick}
                    >
                        {user !== null ? <h4>{user.displayName}</h4> : <b>User</b>}
                    </Button>
                    <Menu
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        MenuListProps={{
                            'aria-labelledby': 'basic-button',
                        }}
                    >
                        <MenuItem href='/'>Profile</MenuItem>
                        <MenuItem onClick={logoutGoogle}>Logout</MenuItem>
                    </Menu></h5>
            </div>

            <div className="mt-5">
                {listMenu.map((item, index) => {
                    return (
                        <div className="row mt-4" key={index} >
                            <h4 ><a href={item.url} className="menu linkStyle" ><b>{item.name}</b></a></h4>
                        </div>
                    )
                })}
            </div>

            <div className="row mt-5" style={user ? { display: "none" } : { display: "" }}>
                <h5 onClick={loginClick} className="menu"> <p className="linkStyle" ><i className="fa-solid fa-lock"></i> Log in</p> </h5>
            </div>


            {/* <div className="row discountStyle"   >
                <Button className="buttonHome" variant="warning"> % Discount %</Button>
            </div>
            <div className="row mt-4">
                <Button className="buttonHome" variant="dark">  New this week</Button>
            </div> */}

            <div className="searchIcon">
                <div className="row " style={{ marginTop: "80px" }} >
                    <div className="menu " >
                        <a onClick={handleSearch} className="linkIconStyle"> <i className="fa-solid fa-magnifying-glass iconStyle" ></i>&ensp; &ensp;<h5>SEARCH</h5></a>
                    </div>
                </div>
                <div className="row mt-4" >
                    <div className="menu" >
                        <a href="/cart" className="linkIconStyle" > <i className="fa-solid fa-cart-shopping iconStyle" ></i>&ensp; &ensp;<h5>CART(  <span style={{ color: "orange", fontWeight: "bolder" }}>{`${cartCount}`}</span> )</h5></a>
                    </div>
                </div>
                <div className="row mt-4" >
                    <div className="menu" >
                        <a href="/myorder" className="linkIconStyle" > <i className="fa-solid fa-list-check iconStyle" ></i>&ensp; &ensp;<h5>MY ORDER</h5></a>
                    </div>
                </div>
            </div>
            <div className="row iconHeaderStyle" >
                {iconContact.map((item, index) => {
                    return (
                        <div className="col-2 social" key={index}>
                            <i className={item.icon} ></i>
                        </div>
                    )
                })}
            </div>

            <LogInForm modalSign={modalSign}
                setModal={setModal}
                closeModal={closeModal} />
        </div>
    )
}
export default Header;