import { Button } from "react-bootstrap";
import { Card, CardActionArea, CardContent, CardMedia, FormControl, Grid, IconButton, InputLabel, MenuItem, Pagination, Select, Slider, Snackbar, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import CloseIcon from '@mui/icons-material/Close';
import { useDispatch, useSelector } from "react-redux";
import { Box } from "@mui/system";
import { ChangeNoPage, FilterAction } from "../actions/getListDataAction";
import { useNavigate } from "react-router-dom";
import { FilterCategoriesAction, FilterColorAction, FilterMaxPriceAction, FilterMinPriceAction, FilterNameAction } from "../actions/FilterAction";
import './style.css'
import Header from "./Header";

const Shop = () => {
    const dispatch = useDispatch()
    const callNaviage = useNavigate();

    const { products } = useSelector((reduxData) => reduxData.ListProductReducer);
    const { filterMaxPrice, filterMinPrice, filterName, filterColor, filterCategories } = useSelector((reduxData) => reduxData.FilterReducer)
    const { currentPage, noPage } = useSelector((reduxData) => reduxData.ListProductReducer)

    const [classCate, setClassCate] = useState('')
    const [cartItem, setCartItem] = useState([])
    const [age, setAge] = React.useState('');
    const [value, setValue] = React.useState([0, 350]); //value price
    //alert
    const [open, setOpen] = React.useState(false);
    const [messageInfo, setMessageInfo] = React.useState(undefined);

    const catagories = ["Chairs", "Beds", "Accesories", "Furniture", "Home Deco", "Dressings", "Tables"];
    const type = ['Amado', 'Ikea', 'Artdeco', 'The factory', 'Furniture Inc']
    const color = ['red', 'blue', 'gray', 'brow', 'black', 'white', 'orange', 'yellow']

    const limit = 6;

    var cartParse = localStorage.getItem('cart');
    var carts = []
    if (cartParse !== null && cartParse !== [] && cartParse !== "" && cartParse.length !== 0) {
        carts = JSON.parse(cartParse)
    } else {  }

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    const handleChangePrice = (event, newValue) => {
        setValue(newValue);
        dispatch(FilterMinPriceAction(newValue[0]))
        dispatch(FilterMaxPriceAction(newValue[1]))
    };

    const detailProduct = (id) => {
        callNaviage(`/detail/${id}`)
    }

    //filter
    const handleNameFilter = (value) => {
        dispatch(FilterNameAction(value.target.value))
    }
    const FilterClick = () => {
        dispatch(FilterAction(limit, currentPage, filterMaxPrice, filterMinPrice, filterName, filterColor, filterCategories))
    }
    const onfilterCategories = (valueCategories, index) => {
        dispatch(FilterCategoriesAction(valueCategories));
        setClassCate(index)
    }
    const FilterColor = (valueColor) => {
        dispatch(FilterColorAction(valueColor.target.value))
    }

    // add Product
    const addProductToCart = (product) => {
        const exist = carts.find((itemmm) => itemmm._id === product._id)
        if (exist) {
            const newCart = carts.map((item) => item._id === product._id ? { ...exist, qty: exist.qty + 1 } : item);
            setCartItem(newCart)
            localStorage.setItem('cart', JSON.stringify(newCart))
            setOpen(true)
        } else {
            const newCart = [...carts, { ...product, qty: 1 }]
            setCartItem(newCart)
            localStorage.setItem('cart', JSON.stringify(newCart))
            setOpen(true)
        }
    }

    const changePageHandler = (event, value) => {
        dispatch(ChangeNoPage(value))
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    const handleExited = () => {
        setMessageInfo(undefined);
    };

    useEffect(() => {
        dispatch(FilterAction(limit, currentPage, filterMaxPrice, filterMinPrice, filterName, filterColor, filterCategories))
    }, [currentPage])

    return (
        <>
            <div className="row" >
                <Header />

                <div className="col-sm-2 Categories " >
                    <h4 >Categories</h4>
                    <div className="listCate">
                        <ul style={{ listStyleType: "none" }}>
                            {catagories.map((product, index) => {
                                return (
                                    <li key={index} onClick={() => onfilterCategories(product, index)} >
                                        <h6 style={classCate === index ? { color: "orange", cursor: "pointer", fontSize: "larger" } : { color: "black", cursor: "pointer" }} className="categoriesHover">{product} </h6>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>

                    <h4 >Brand</h4>
                    <Box sx={{ minWidth: 120 }}>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Brand</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={age}
                                label="Brand"
                                onChange={handleChange}
                            >
                                {type.map((item, index) => {
                                    return (
                                        <MenuItem value={item} key={index}>{item}</MenuItem>
                                    )
                                })}
                            </Select>
                        </FormControl>
                    </Box>

                    <h4 style={{ marginTop: "90px" }}>Color</h4>
                    <Box sx={{ minWidth: 120 }}>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Color</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={filterColor}
                                label="Brand"
                                onChange={FilterColor}
                            >
                                {color.map((item, index) => {
                                    return (
                                        <MenuItem value={item} key={index}>{item}</MenuItem>
                                    )
                                })}
                            </Select>
                        </FormControl>
                    </Box>

                    <h4 style={{ marginTop: "90px" }}>Price</h4>
                    <Box sx={{ width: "100%" }}>
                        <Slider
                            value={value}
                            onChange={handleChangePrice}
                            valueLabelDisplay="auto"
                            min={0}
                            max={350}
                        />
                        <div style={{ display: "flex", justifyContent: "space-between" }} >
                            <div>{value[0]}</div>
                            <div>{value[1]}</div>
                        </div>

                    </Box>

                    <h4 className="mt-5" id="search"> Name </h4>
                    <TextField label="Name" onChange={handleNameFilter} value={filterName} fullWidth></TextField>
                    <Button onClick={FilterClick} variant="warning" className="mt-5 ">FILTER</Button>
                </div>

                <div className="col-sm-7 setItem" >
                    <div className="row">
                        {products ? products.map((product, index) => {
                            return (
                                <div className="col-5 shopImg" key={index}>
                                    <Card >
                                        <CardActionArea item={product} >
                                            <CardMedia
                                                width='120%'
                                                component="img"
                                                height="500"
                                                image={product.imageUrl}
                                                alt={index}
                                                onClick={() => detailProduct(product._id)}
                                            />
                                            <CardContent>
                                                <div style={{
                                                    marginTop: "15px",
                                                    width: "80px", height: "3px",
                                                    backgroundColor: "orange",
                                                    marginBottom: "15px",
                                                }}></div>

                                                <h5>From ${product.buyPrice}</h5>
                                                <div style={{ display: "flex", justifyContent: "space-between" }}>
                                                    <h4 id="searchName"><b>{product.name}</b></h4>
                                                    <i className="fa-solid fa-cart-shopping shopIcon" onClick={() => addProductToCart(product)}></i>
                                                </div>
                                            </CardContent>
                                        </CardActionArea>
                                    </Card>
                                </div>
                            )
                        }) : null}

                        <Grid container mt={3} justifyContent="flex-end">
                            <Grid item>
                                <Pagination count={noPage} defaultPage={1} onChange={changePageHandler} color="primary" variant="outlined" shape="rounded" />
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>
            <div>
                <Snackbar
                    key=""
                    open={open}
                    autoHideDuration={1000}
                    onClose={handleClose}
                    TransitionProps={{ onExited: handleExited }}
                    message="Product has been added to your cart"
                    action={
                        <React.Fragment>
                            <IconButton
                                aria-label="close"
                                color="info"
                                sx={{ p: 0.5 }}
                                onClick={handleClose}
                            >
                                <CloseIcon />
                            </IconButton>
                        </React.Fragment>
                    }
                />
            </div>
        </>
    )
}
export default Shop;