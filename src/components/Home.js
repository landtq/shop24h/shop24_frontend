import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListData } from "../actions/getListDataAction";
import { useNavigate } from "react-router-dom";
import './style.css'
import Header from "./Header";
import Subcrible from "./Subcribe";

const Home = () => {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const limit = 9;
    const { products } = useSelector((reduxData) => reduxData.ListProductReducer);

    const handleDetailFromHome = (id) => {
        navigate(`/detail/${id._id}`)
    }

    useEffect(() => {
        dispatch(getListData(limit))
    }, []);

    return (
        <>
            <div className="row" >
                <Header />
                <div className="col-sm-9">
                    <div className="row pImg">
                        {products.map((product, index) => {
                            return (
                                <div onClick={() => handleDetailFromHome(product)} className="col-sm-4 heightImg" style={{ backgroundImage: `url(${product.imageUrl})`, backgroundPosition: "center" }} key={index}>
                                    <div className="line"></div>
                                    <h5>From ${product.buyPrice}</h5>
                                    <h4><b>{product.name}</b></h4>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
           <Subcrible/>
        </>
    )
}
export default Home;