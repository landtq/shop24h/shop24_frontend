import { Button } from "react-bootstrap";
import { FormGroup, Input, Label, Modal, ModalHeader } from "reactstrap"
import React, { useState } from "react";
import Header from "./Header";
import Subcrible from "./Subcribe";
import { useDispatch } from "react-redux";
import { createNewOrder } from "../actions/CheckOutAction";
import { Alert, Snackbar, Stack } from "@mui/material";
import { useNavigate } from "react-router-dom";

const CheckOut = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    const [dataUser, setDataUser] = useState({
        city: "",
        address: "",
        country: "",
        email: "",
        fullName: "",
        phone: 0
    })
    const [note, setNote] = useState("")
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const closeAlert = () => {
        setDisplayAlert(false);
    }
    //cartTotal
    var cartTotalParse = localStorage.getItem('cartTotal');
    var cartTotals = []
    if (cartTotalParse !== null && cartTotalParse !== [] && cartTotalParse !== "" && cartTotalParse.length !== 0) {
        cartTotals = JSON.parse(cartTotalParse)
    } else { console.log(cartTotalParse) }
    //cart
    var cartParse = localStorage.getItem('cart');
    var carts = []
    if (cartParse !== null && cartParse !== [] && cartParse !== "" && cartParse.length !== 0) {
        carts = JSON.parse(cartParse)
    } else { console.log(cartParse) }

    const ipnChange = (event) => {
        if(event) {
            setDataUser({
                ...dataUser,
                [event.target.name]: event.target.value
            })
        } else {
            setDataUser({
          
            })
        }
    }
    const inChangeNote = (e) => {
        setNote(e.target.value)
    }

    const handleCheckOut = () => {
        var today = new Date();
        var todasdy = new Date();
        var deliveryDay = today.getDate() + 7;
        todasdy.setDate(deliveryDay)
        var dataOrder = {
            orderDate: today.toUTCString(),
            shippedDate: todasdy.toUTCString(),
            note: note,
            cost: cartTotals.priceee
        }
        var validateInp = handleInpValue()
        if (validateInp) {
            dispatch(createNewOrder(dataUser, dataOrder, carts,modal, setModal, navigate))
            setDataUser({
                city: "",
                address: "",
                country: "",
                email: "",
                fullName: "",
                phone: 0
            })
            setNote("")
        }
    }
    const handleInpValue = () => {
        if (dataUser.fullName === "") {
            setDisplayAlert(true);
            settextAlert("please enter full name");
            return false;
        }
        if (!dataUser.email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            setDisplayAlert(true);
            settextAlert("email is invalid");
            return false;
        }
        if (isNaN(dataUser.phone) || dataUser.phone <= 0) {
            setDisplayAlert(true);
            settextAlert("phone is invalid");
            return false;
        }
        if (dataUser.address === "") {
            setDisplayAlert(true);
            settextAlert("please enter address");
            return false;
        }
        if (dataUser.city === "") {
            setDisplayAlert(true);
            settextAlert("please enter city");
            return false;
        }
        if (dataUser.country==="") {
            setDisplayAlert(true);
            settextAlert("please enter country");
            return false;
        }
        if (note === "") {
            setDisplayAlert(true);
            settextAlert("please enter note");
            return false;
        }
        return true
    }

    return (
        <>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity="error">{textAlert} </Alert>
                </Stack>
            </Snackbar>
            <div>
                <Modal isOpen={modal} toggle={toggle} style={{ display: "flex", justifyContent: "end " }}>
                    <ModalHeader toggle={toggle}><img src="https://img.icons8.com/arcade/64/000000/ok.png" alt=""/>YOUR ORDER WAS SUCCESSFUL!</ModalHeader>
                </Modal>
            </div>
            <div className="row" >
                <Header />
                <div className="col-sm-5 checkout" >
                    <h1> Checkout</h1>
                    <div className="row mt-4">
                        <Input placeholder="Full Name" className="inpCheckOut" name="fullName" onChange={ipnChange} value={dataUser.fullName}/>
                    </div>
                    <div className="row mt-4">
                        <Input placeholder="Email" className="inpCheckOut" name="email" onChange={ipnChange} value={dataUser.email}/>
                    </div>
                    <div className="row mt-4">
                        <Input placeholder="Phone" className="inpCheckOut" name="phone" onChange={ipnChange} value={dataUser.phone}/>
                    </div>
                    <div className="row mt-4">
                        <Input placeholder="Adress" className="inpCheckOut" name="address" onChange={ipnChange} value={dataUser.address}/>
                    </div>
                    <div className="row mt-4">
                        <Input placeholder="City" className="inpCheckOut" name="city" onChange={ipnChange} value={dataUser.city}/>
                    </div>
                    <div className="row mt-4">
                        <Input placeholder="Country" className="inpCheckOut" name="country" onChange={ipnChange} value={dataUser.country}/>
                    </div>
                    <div className="row mt-4">
                        <Input type="textarea" placeholder="Leave a comment about your order" className="inpCheckOut" style={{ height: '170px' }} onChange={inChangeNote} value={note}/>
                    </div>
                </div>

                {/* Cart Total */}
                <div className="col-sm-3 cartTotal" >
                    <div className="container bg-light float-left maginTopCart">
                        <h3> Cart Total</h3>
                        <div className="row mt-5" >
                            <div className="col-sm-9">
                                <h6> Items: </h6>
                            </div>
                            <div className="col-sm-3">
                                <h6>  {cartTotals.itemsss}</h6>
                            </div>
                        </div>
                        <div className="row mt-5" >
                            <div className="col-sm-9">
                                <h6> Tax Price </h6>
                            </div>
                            <div className="col-sm-3">
                                <h6> ${cartTotals.taxxx}.0</h6>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-sm-9">
                                <h6> Delivery: </h6>
                            </div>
                            <div className="col-sm-3">
                                <h6> ${cartTotals.deliveryyy}.0</h6>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-sm-9">
                                <h6> TOTAL:  </h6>
                            </div>
                            <div className="col-sm-3">
                                <h6>
                                    $ {cartTotals.priceee}.0
                                </h6>
                            </div>
                        </div>
                        <FormGroup className="mt-4 check" >
                            <Input
                                type="checkbox"
                                checked
                            />
                            {' '}
                            <Label >
                                Cash on Delivery
                            </Label>
                        </FormGroup>
                        <FormGroup className="mt-2 check" >
                            <Input
                                type="checkbox"
                            />
                            {' '}
                            <Label >
                                Other payment methods <img src="https://img.icons8.com/emoji/30/000000/credit-card-emoji.png" alt=""/><img src="https://img.icons8.com/external-those-icons-lineal-color-those-icons/24/000000/external-Master-Card-payment-methods-those-icons-lineal-color-those-icons.png" alt=""/>
                            </Label>
                        </FormGroup>
                        <div className="row checkOutBtn">
                            <Button className="" variant="warning" onClick={handleCheckOut}>Checkout</Button>
                        </div>
                    </div>
                </div>
            </div>
            <Subcrible />
        </>
    )
}
export default CheckOut;