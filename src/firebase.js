// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getAuth } from "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBc3L_qmhK_L7hjWlZRtMeRjspwbg1Jw6k",
    authDomain: "shop24h-8abf7.firebaseapp.com",
    projectId: "shop24h-8abf7",
    storageBucket: "shop24h-8abf7.appspot.com",
    messagingSenderId: "84869986226",
    appId: "1:84869986226:web:757efd6edcadecffdd56ab"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export default auth;