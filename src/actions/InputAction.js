import { GOOGLE_LOGIN, INPUT_PASSWORD, INPUT_USER_NAME } from "../constants/Input";

export  function InputUserName(value) {
    return{
        type: INPUT_USER_NAME,
        payload: value
    }
}

export  function InputPassword(value) {
    return{
        type: INPUT_PASSWORD,
        payload: value
    }
}

export  function GoogleLogin(value) {
    return{
        type: GOOGLE_LOGIN,
        payload: value
    }
}