import { CREATE_NEW_ORDER } from "../constants/CheckOutConst";

export const createNewOrder = (dataUser, dataOrder, carts,modal, setModal, navigate) => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        const response = await fetch(`http://localhost:8000/customers`, requestOptions);
        const data = await response.json();
        const result = data?.customer.filter(item =>
            item.email === dataUser.email
        );
        localStorage.setItem('cusExit', JSON.stringify(result))
        //cus
        if (result.length === 0 || result === "" || result === []) {
            var myHeadersCus = new Headers();
            myHeadersCus.append("Content-Type", "application/json");
            var requestOptionsCus = {
                method: 'POST',
                redirect: 'follow',
                body: JSON.stringify(dataUser),
                headers: myHeadersCus
            }
            const responseCus = await fetch(`http://localhost:8000/customers`, requestOptionsCus);
            const dataCus = await responseCus.json();

            //order
            if (dataCus.newCustomer) {
                var cusId = dataCus.newCustomer._id
                let myHeadersOrder = new Headers();
                myHeadersOrder.append("Content-Type", "application/json");
                let requestOptionsOrder = {
                    method: 'POST',
                    redirect: 'follow',
                    body: JSON.stringify(dataOrder), 
                    headers: myHeadersOrder
                }
                const responseOrder = await fetch(`http://localhost:8000/customers/${cusId}/orders`, requestOptionsOrder);
                const dataOrderResult = await responseOrder.json();
                //orderdetail 
                if (dataOrderResult) {
                    var orderId = dataOrderResult.data._id
                    carts.map(async item => {
                        var dataOrderDetail = {
                            product: item._id,
                            quantity: item.qty
                        }
                        var myHeadersDetail = new Headers();
                        myHeadersDetail.append("Content-Type", "application/json");
                        var requestOptionsDetail = {
                            method: 'POST',
                            redirect: 'follow',
                            body: JSON.stringify(dataOrderDetail),
                            headers: myHeadersDetail
                        }
                        const responseDetail = await fetch(`http://localhost:8000/orders/${orderId}/order-details`, requestOptionsDetail);
                        const dataDeatil = await responseDetail.json();
                  
                     
                        setModal(true);
                        localStorage.setItem("cart", []);
                        localStorage.setItem("cartTotal", []);
                        localStorage.setItem("newCus", []);
                        localStorage.setItem("newOrder", []);
                     
                    })
                    console.log("create successfull")
                }
                // if(modal=== false) {
                //     navigate("/");
                //     setModal(true);
                // }

            //    await setModal(true);
            //     localStorage.setItem("cart", []);
            //     localStorage.setItem("cartTotal", []);
            //     localStorage.setItem("newCus", []);
            //     localStorage.setItem("newOrder", []);
            }

        } else {
            var cusIdLocal = JSON.parse(localStorage.getItem('cusExit'));
            let cusId = cusIdLocal[0]._id
            var myHeadersOrder = new Headers();
            myHeadersOrder.append("Content-Type", "application/json");
            var requestOptionsOrder = {
                method: 'POST',
                redirect: 'follow',
                body: JSON.stringify(dataOrder),
                headers: myHeadersOrder
            }
            const responseOrder = await fetch(`http://localhost:8000/customers/${cusId}/orders`, requestOptionsOrder);
            const dataOrderResult = await responseOrder.json();
            //orderdetail 
            if (dataOrderResult) {
                let orderId = dataOrderResult.data._id
                carts.map(async item => {
                    var dataOrderDetail = {
                        product: item._id,
                        quantity: item.qty
                    }
                    var myHeadersDetail = new Headers();
                    myHeadersDetail.append("Content-Type", "application/json");
                    var requestOptionsDetail = {
                        method: 'POST',
                        redirect: 'follow',
                        body: JSON.stringify(dataOrderDetail),
                        headers: myHeadersDetail
                    }
                    const responseDetail = await fetch(`http://localhost:8000/orders/${orderId}/order-details`, requestOptionsDetail);
                    const dataDeatil = await responseDetail.json();
                })
                console.log("add order successfull")
            }
            setModal(true)
            localStorage.setItem("cart", []);
            localStorage.setItem("cartTotal", []);
            localStorage.setItem("newCus", []);
            localStorage.setItem("newOrder", []);
        }
        return dispatch({
            type: CREATE_NEW_ORDER,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}


