import { FILTER_PRODUCT, GET_DATA_PRODUCT, GET_DATA_PRODUCT_HOME } from "../constants/Product";
import { PAGINATION } from '../constants/Product'

export const getListData = (limit) => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        const response = await fetch(`http://localhost:8000/productRouters/?limit=${limit}`, requestOptions);
        const data = await response.json();
        return dispatch({
            type: GET_DATA_PRODUCT_HOME,
            payload: data.product
        });
    }
    catch (error) {
        console.log(error)
    }
}
export const ChangeNoPage = (value) => {
    return {
        type: PAGINATION,
        payload: value
    }
}
export const FilterAction = (limit,currentPage, filterMaxPrice, filterMinPrice, filterName, filterColor, filterCategories) => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        if (filterMaxPrice === 350 && filterMinPrice === 0 && filterName === "" && filterColor === "" && filterCategories === "") {
            const response = await fetch(`http://localhost:8000/productRouters?limit=${limit}&&page=${currentPage}`, requestOptions);
            const data = await response.json();
            return dispatch({
                type: FILTER_PRODUCT,
                payload: {
                    total: data.total,
                    limit: data.limit,
                    product: data.product,
                }
            });
        } else if (filterMaxPrice !== 350 || filterMinPrice !== 0 || filterName !== "" || filterColor !== "" || filterCategories !== "") {
            const response = await fetch(`http://localhost:8000/productRouters/?filterMaxPrice=${filterMaxPrice}&&filterMinPrice=${filterMinPrice}&&filterName=${filterName}&&filterColor=${filterColor}&&filterCategories=${filterCategories}&&limit=${limit}&&page=${currentPage}`, requestOptions);
            const data = await response.json();
            console.log(data)
            return dispatch({
                type: FILTER_PRODUCT,
                payload: {
                    total: data.total,
                    limit: data.limit,
                    product: data.product,
                }
            });
        }
    }
    catch (error) {
        console.log(error)
    }
}
