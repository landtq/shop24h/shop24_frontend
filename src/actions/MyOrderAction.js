import { GET_LIST_ORDER } from "../constants/MyOrder";

export const getListOrder = () => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        const response = await fetch(`http://localhost:8000/orders`, requestOptions);
        const data = await response.json();
        return dispatch({
            type: GET_LIST_ORDER,
            payload: data.data
        });
    }
    catch (error) {
        console.log(error)
    }
}