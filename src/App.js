import 'bootstrap/dist/css/bootstrap.min.css'

import { Route, Routes } from 'react-router-dom';
import HomePage from './views/HomePage';
import ListPage from './views/ListPage';
import DetailPage from './views/DetailPage';
import CartPage from './views/CartPage';
import CheckOutPage from './views/CheckOutPage';
import Footer from './components/Footer';
import MyOrderPage from './views/MyOrderPage';

function App() {
  return (
    <div >
      <Routes>
        <Route exact path='/' element={<HomePage />} />
        <Route exact path='/products' element={<ListPage />} />
        <Route path='/detail/:paramId' element={<DetailPage />} />
        <Route exact path='/cart' element={<CartPage />} />
        <Route exact path='/cart/:paramId' element={<CartPage />} />
        <Route exact path='/checkout' element={<CheckOutPage />} />
        <Route exact path='/myorder' element={<MyOrderPage />} />
      </Routes>
      {/* <a href='#' style={{ position: "fixed", zIndex: "2147483647", display: "block" }}>
        <i className='fa-solid fa-angle-up' >To the top</i>
      </a> */}
      <Footer />
    </div>
  );
}

export default App;
